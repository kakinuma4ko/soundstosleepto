package com.kakinuma.soundstosleepto;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private boolean isRain = false;
    private boolean isBirdsong = false;
    private boolean isCrickets = false;
    private boolean isCampfire = false;
    private boolean isWaves = false;
    private boolean isWind = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);





        // main
        final Button buttonRain = (Button)findViewById(R.id.buttonRain);
        final MediaPlayer mpRain = MediaPlayer.create(this, R.raw.rain);
        //final Uri UriRain = Uri.parse("R.raw.rain");
        final SeekBar seekbarRain = (SeekBar)findViewById(R.id.seekBarRain);
        mpRain.setLooping(true);
        buttonRain.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mpRain.create(MainActivity.this,UriRain);
               /*
               01-02 19:25:26.885 30347-30363/com.kakinuma.soundstosleepto W/MediaPlayer-JNI: MediaPlayer finalized without being released
               01-02 19:25:28.540 3844-31053/? E/: Failed to open file 'R.raw.rain'. (No such file or directory)
               01-02 19:25:28.540 30347-30369/com.kakinuma.soundstosleepto E/MediaPlayer: error (1, -2147483648)
               01-02 19:25:28.550 30347-30347/com.kakinuma.soundstosleepto E/MediaPlayer: Should have subtitle controller already set
                */
                if (isRain == false) {
                    try {
                        if (mpRain != null) {
                            mpRain.stop();
                        }
                        //mpRain.create(this,R.raw.rain);
                        mpRain.prepare();
                        mpRain.start();
                        isRain = true;
                        Snackbar.make(v, "Rain start", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    } catch (Exception e) {
                        Snackbar.make(v, "Rain Exception 1", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                } else {
                    try {
                        if (mpRain != null) {
                            //mpRain.reset();
                            mpRain.stop();
                            //mpRain.pause();
                            //mpRain.release();
                            Snackbar.make(v, "Rain stop", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    } catch (Exception e) {
                        Snackbar.make(v, "Rain Exception 2", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                    isRain = false;
                }
            }
        });
        seekbarRain.setMax(10);
        seekbarRain.setProgress(5);
        seekbarRain.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() //调音监听器
        {
            public void onProgressChanged(SeekBar arg0, int progress, boolean fromUser) {
                //
                //buttonRain.setText(seekbarRain.getProgress());

                //int tempvol = seekbarRain.getProgress();
                //float tempvolF = tempvol/10.0f;
                mpRain.setVolume(seekbarRain.getProgress() / 10.0f, seekbarRain.getProgress() / 10.0f);
                //Log.i("seek", "seekbarRain.getProgress()" + tempvolF);
                //mpRain.setVolume(seekbarRain.getProgress()/10,seekbarRain.getProgress()/10);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub


            }
        });









        final Button buttonBirdsong = (Button)findViewById(R.id.buttonBirdsong);
        final MediaPlayer mpBirdsong = MediaPlayer.create(this, R.raw.birdsong);
        final SeekBar seekbarBirdsong = (SeekBar)findViewById(R.id.seekBarBirdsong);
        mpBirdsong.setLooping(true);
        buttonBirdsong.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBirdsong == false) {
                    try {
                        if (mpBirdsong != null) {
                            mpBirdsong.stop();
                        }
                        mpBirdsong.prepare();
                        mpBirdsong.start();
                        isBirdsong = true;
                    } catch (Exception e) {

                    }
                } else {
                    try {
                        if (mpBirdsong != null) {
                            mpBirdsong.stop();
                        }
                    } catch (Exception e) {

                    }
                    isBirdsong = false;
                }
            }
        });
        seekbarBirdsong.setMax(10);
        seekbarBirdsong.setProgress(5);
        seekbarBirdsong.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() //调音监听器
        {
            public void onProgressChanged(SeekBar arg0, int progress, boolean fromUser) {

                mpBirdsong.setVolume(seekbarBirdsong.getProgress() / 10.0f, seekbarBirdsong.getProgress() / 10.0f);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub


            }
        });








        final Button buttonCrickets = (Button)findViewById(R.id.buttonCrickets);
        final MediaPlayer mpCrickets = MediaPlayer.create(this, R.raw.crickets);
        final SeekBar seekbarCrickets = (SeekBar)findViewById(R.id.seekBarCrickets);
        mpCrickets.setLooping(true);
        buttonCrickets.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCrickets == false) {
                    try {
                        if (mpCrickets != null) {
                            mpCrickets.stop();
                        }
                        mpCrickets.prepare();
                        mpCrickets.start();
                        isCrickets = true;
                    } catch (Exception e) {

                    }
                } else {
                    try {
                        if (mpCrickets != null) {
                            mpCrickets.stop();
                        }
                    } catch (Exception e) {

                    }
                    isCrickets = false;
                }
            }
        });
        seekbarCrickets.setMax(10);
        seekbarCrickets.setProgress(5);
        seekbarCrickets.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() //调音监听器
        {
            public void onProgressChanged(SeekBar arg0, int progress, boolean fromUser) {

                mpCrickets.setVolume(seekbarCrickets.getProgress() / 10.0f, seekbarCrickets.getProgress() / 10.0f);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub


            }
        });








        final Button buttonCampfire = (Button)findViewById(R.id.buttonCampfire);
        final MediaPlayer mpCampfire = MediaPlayer.create(this, R.raw.campfire);
        final SeekBar seekbarCampfire = (SeekBar)findViewById(R.id.seekBarCampfire);
        mpCampfire.setLooping(true);
        buttonCampfire.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCampfire == false) {
                    try {
                        if (mpCampfire != null) {
                            mpCampfire.stop();
                        }
                        mpCampfire.prepare();
                        mpCampfire.start();
                        isCampfire = true;
                    } catch (Exception e) {

                    }
                } else {
                    try {
                        if (mpCampfire != null) {
                            mpCampfire.stop();
                        }
                    } catch (Exception e) {

                    }
                    isCampfire = false;
                }
            }
        });
        seekbarCampfire.setMax(10);
        seekbarCampfire.setProgress(5);
        seekbarCampfire.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() //调音监听器
        {
            public void onProgressChanged(SeekBar arg0, int progress, boolean fromUser) {

                mpCampfire.setVolume(seekbarCampfire.getProgress() / 10.0f, seekbarCampfire.getProgress() / 10.0f);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub


            }
        });




        final Button buttonWaves = (Button)findViewById(R.id.buttonWaves);
        final MediaPlayer mpWaves = MediaPlayer.create(this, R.raw.waves);
        final SeekBar seekbarWaves = (SeekBar)findViewById(R.id.seekBarWaves);
        mpWaves.setLooping(true);
        buttonWaves.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isWaves == false) {
                    try {
                        if (mpWaves != null) {
                            mpWaves.stop();
                        }
                        mpWaves.prepare();
                        mpWaves.start();
                        isWaves = true;
                    } catch (Exception e) {

                    }
                } else {
                    try {
                        if (mpWaves != null) {
                            mpWaves.stop();
                        }
                    } catch (Exception e) {

                    }
                    isWaves = false;
                }
            }
        });
        seekbarWaves.setMax(10);
        seekbarWaves.setProgress(5);
        seekbarWaves.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() //调音监听器
        {
            public void onProgressChanged(SeekBar arg0, int progress, boolean fromUser) {

                mpWaves.setVolume(seekbarWaves.getProgress() / 10.0f, seekbarWaves.getProgress() / 10.0f);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub


            }
        });




        final Button buttonWind = (Button)findViewById(R.id.buttonWind);
        final MediaPlayer mpWind = MediaPlayer.create(this, R.raw.wind);
        final SeekBar seekbarWind = (SeekBar)findViewById(R.id.seekBarWind);
        mpWind.setLooping(true);
        buttonWind.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isWind == false) {
                    try {
                        if (mpWind != null) {
                            mpWind.stop();
                        }
                        mpWind.prepare();
                        mpWind.start();
                        isWind = true;
                    } catch (Exception e) {

                    }
                } else {
                    try {
                        if (mpWind != null) {
                            mpWind.stop();
                        }
                    } catch (Exception e) {

                    }
                    isWind = false;
                }
            }
        });
        seekbarWind.setMax(10);
        seekbarWind.setProgress(5);
        seekbarWind.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() //调音监听器
        {
            public void onProgressChanged(SeekBar arg0, int progress, boolean fromUser) {

                mpWind.setVolume(seekbarWind.getProgress() / 10.0f, seekbarWind.getProgress() / 10.0f);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub


            }
        });










    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camara) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
